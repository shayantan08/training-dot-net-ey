﻿using System;

namespace Shayantan_foreach_100822
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] carCompanies = { "Tata Motors", "Mahindra", "Volkswagen", "Toyota" };

            foreach (string car in carCompanies)
            {
                Console.Write(car +" | ");
            }
        }
    }
}
