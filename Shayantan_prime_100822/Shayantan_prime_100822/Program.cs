﻿using System;

namespace Shayantan_prime_100822
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = Convert.ToInt32(Console.ReadLine());
            if (!(((a % 1) == 0) && ((a % a) == 0))){
                Console.WriteLine("Prime");
            }
        }
    }
}
