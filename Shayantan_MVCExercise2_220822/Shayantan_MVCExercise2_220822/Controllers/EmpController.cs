﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shayantan_MVCExercise2_220822.Models;

namespace Shayantan_MVCExercise2_220822.Controllers
{
    public class EmpController : Controller
    {
        private static List<EmpModel> student = new List<EmpModel>()
        {
            new EmpModel {eid = Guid.NewGuid(), ename = "Sam",dept = "Analyst"},
            new EmpModel {eid = Guid.NewGuid(), ename = "Sam",dept = "Analyst"},
        };
        public IActionResult Index()
        {
            
            return View();
        }

        public ActionResult Details()
        {
            return View(student);
        }

        public ActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]

        public ActionResult AddStudent(EmpModel input)
        {
            student.Add(new EmpModel { eid = Guid.NewGuid(), ename = input.ename, dept = input.dept });
            return View();
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            student.Remove(student.SingleOrDefault(o => o.eid == id));
            return RedirectToAction("Details");
            ViewBag.message = "Edited Successfully";
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            return View(student.Single(o => o.eid == id));
        }

        [HttpPost]

        public ActionResult Edit(Guid id, EmpModel input)
        {
            //var stude = student.Where(x => x.Sid == s.Sid).Sig();
            var e = student.Where(x => x.eid == id).SingleOrDefault();
            student.Remove(e);
            student.Add(new EmpModel { eid = id, ename = input.ename, dept = input.dept});

            return View();
        }
    }
}
