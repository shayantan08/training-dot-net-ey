﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shayantan_MVCExercise2_220822.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_MVCExercise2_220822.Controllers
{
    [Route("[controller]/[action]")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [Route("~/Sample")]
        public IActionResult Index()
        {
            ViewBag.message = "Hello";
            ViewData["message1"] = "Welcome to";
            TempData["message2"] = "This site";
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
