﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_MVCExercise2_220822.Models
{
    public class EmpModel
    {
        public Guid eid { get; set; }
        public string ename { get; set; }
        public string dept { get; set; }
    }
}
