﻿using System;

namespace Shayantan_Collections_120822
{
    class Emp
    {
        public int id = 0;
        public string empname = string.Empty;

        public int Eid { 
            get { return id; }
            set { id = value; } 
        }

        public String Empname {
            get { return empname; }
            set { empname = value; } 
        }

        public Emp(int Eid, string Empname)
        {
            id = Eid;
            empname = Empname;

        } 
    }

    
    class Program
    {
        static void Main(string[] args)
        {
            Emp e = new Emp(1, "Hello");
            Console.WriteLine(e.id+e.empname);

        }
    }
}
