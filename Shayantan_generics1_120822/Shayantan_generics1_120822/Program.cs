﻿using System;

namespace Shayantan_generics1_120822
{
    public class SClass
    {
        public void GenericMethod<T>(T Param1, T Param2)
        {
            Console.WriteLine($"Param 1: {Param1} & Param 2: {Param2}");
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generic Example");
            SClass s = new SClass();
            s.GenericMethod<int>(10, 29);
            s.GenericMethod(10.5, 29.5);
            s.GenericMethod<string>("Hello","World");


        }
    }
}
