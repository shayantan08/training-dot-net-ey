﻿using System;
using System.Collections;

namespace Shayantan_ienum_120822
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add("abc");
            list.Add(2);
            list.Add(12.4);
            list.Add('c');
            list.Add(true);

            IEnumerator IEnum = list.GetEnumerator();

            string msg = " ";
            while (IEnum.MoveNext())
            {
                msg += IEnum.Current.ToString() + "\n";
            }

            Console.WriteLine(msg + "GetEnumerator on ArrayList");

        }
    }
}
