﻿using Shayantan_CQRSLib_260822.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shayantan_CQRSLib_260822.DataAccess
{
    public interface IDataAccess
    {
        public void AddStudent(int id, string name);
        Student GetStudentById(int id);
        List <Student> GetStudents();
        //Task<Student> GetStudentById();
    }
}
