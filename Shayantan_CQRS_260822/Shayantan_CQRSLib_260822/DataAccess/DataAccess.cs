﻿using Shayantan_CQRSLib_260822.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Shayantan_CQRSLib_260822.DataAccess
{
    public class DataAccess:IDataAccess
    {
        List<Student> stud = new List<Student>();

        public void AddStudent(int id, string name)
        {
            stud.Add(new Student
            {
                Sid = id,
                Sname = name
            });
        }

        public Student GetStudentById(int id)
        {
            return stud.SingleOrDefault(c => c.Sid == id);
        }
        public List<Student> GetStudents()
        {
            return stud;
        }
    }
}
