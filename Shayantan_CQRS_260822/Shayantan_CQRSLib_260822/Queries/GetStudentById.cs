﻿using Shayantan_CQRSLib_260822.Models;
using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Shayantan_CQRSLib_260822.Queries
{
    public record GetStudentById(int id) : IRequest<Student stud>;
}
