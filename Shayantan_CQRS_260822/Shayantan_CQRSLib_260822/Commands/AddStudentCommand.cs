﻿using MediatR;
using Shayantan_CQRSLib_260822.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shayantan_CQRSLib_260822.Commands
{ 
    public record AddStudentCommand : IRequest<Student>;
}
