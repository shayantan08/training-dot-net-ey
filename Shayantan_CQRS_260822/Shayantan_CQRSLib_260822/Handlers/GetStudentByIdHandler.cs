﻿using MediatR;
using Shayantan_CQRSLib_260822.DataAccess;
using Shayantan_CQRSLib_260822.Models;
using Shayantan_CQRSLib_260822.Queries;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_CQRSLib_260822.Handlers
{
    public class GetStudentByIdHandler : IRequestHandler<GetStudentById, Student>
    {
        private readonly IDataAccess _data;
        public GetStudentByIdHandler(IDataAccess data)
        {
            _data = data;
        }
        public Task<Student> Handle(GetStudentById request, CancellationToken cancellationToken)
        {
            return _data.GetStudentById(id);
        }
    }
}
