﻿using MediatR;
using Shayantan_CQRSLib_260822.Commands;
using Shayantan_CQRSLib_260822.DataAccess;
using Shayantan_CQRSLib_260822.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_CQRSLib_260822.Handlers
{
    public class AddStudentHandler : IRequestHandler<AddStudentCommand, Student>
    {
        private readonly IDataAccess _data;
        public AddStudentHandler(IDataAccess data)
        {
            _data = data;
        }
        public Task<Student> Handle(AddStudentCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.AddStudent(request.Sid,request.Sname));
        }
    }
}
