﻿using MediatR;
using Shayantan_CQRSLib_260822.DataAccess;
using Shayantan_CQRSLib_260822.Models;
using Shayantan_CQRSLib_260822.Queries;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_CQRSLib_260822.Handlers
{
    public class GetStudentListHandler : IRequestHandler<GetStudents, List<Student>>
    {
        private readonly IDataAccess _data;
        public GetStudentListHandler(IDataAccess data)
        {
            _data = data;
        }
        public Task<List<Student>> Handle(GetStudents request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.GetStudents);
        }
    }
}
