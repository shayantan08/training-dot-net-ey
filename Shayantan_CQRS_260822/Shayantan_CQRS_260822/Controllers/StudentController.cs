﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Shayantan_CQRSLib_260822.Commands;
using Shayantan_CQRSLib_260822.Models;
using Shayantan_CQRSLib_260822.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Shayantan_CQRS_260822.Controllers
{
    

    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IMediator _mediator;

        public StudentController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<List<Student>> Get()
        {
            return await _mediator.Send(new GetStudents());
        }

        [HttpGet("{id}")]
        public async Task<Student> Get(int id)
        {
            return await _mediator.Send(new GetStudentById(id));
        }

        [HttpPost]
        public async Task<Student> Post([FromBody] Student value)
        {
            var model = new AddStudentCommand(value.Sid, value.Sname);
            return await _mediator.Send(model);
        }
    }
}
