﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_MVCexampleLibrary_180822.Models
{
    public class EmpClass
    {
        public int empid { get; set; }
        public string ename { get; set; }
        public string deptname { get; set; }
    }
}
