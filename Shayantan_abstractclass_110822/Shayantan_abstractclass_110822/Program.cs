﻿using System;

namespace Shayantan_abstractclass_110822
{
    abstract public class triangle
    {
        public int b=2,h=2;
        public abstract void calc();
    }

    public class area : triangle
    {
        public override void calc() {
            double ar = 0.5 * b * h;
            Console.WriteLine("Area of triangle is - " + ar);
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            area t = new area();
            t.calc();
        }
    }
}
