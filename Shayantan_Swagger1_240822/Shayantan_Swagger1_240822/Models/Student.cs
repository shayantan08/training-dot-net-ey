﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_Swagger1_240822.Models
{
    public class Student
    {
        [Required]
        public int Sid { get; set; }
        public string Sname { get; set; }
        public string Dept { get; set; }
    }
}
