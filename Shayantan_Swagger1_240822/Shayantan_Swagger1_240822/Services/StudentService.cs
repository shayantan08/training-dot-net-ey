﻿using Shayantan_Swagger1_240822.IServices;
using Shayantan_Swagger1_240822.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_Swagger1_240822.Services
{
    
    public class StudentService : IStudentService
    {
        List<Student> _student = new List<Student>();
        public StudentService()
        {
            for(int i = 0; i < 3; i++)
            {
                _student.Add(new Student()
                {
                    Sid = i,
                    Sname = "Stu"+i,
                    Dept = "100"+i

                }) ;

            }
        }
        public List<Student> Delete(int studentid)
        {
            _student.RemoveAll(x => x.Sid == studentid);
            return _student;
        }

        public Student Get(int id)
        {
            return _student.SingleOrDefault(x => x.Sid == id);
                
        }

        public List<Student> Gets()
        {
            return _student;
        }

        public List<Student> Save(Student student)
        {
            _student.Add(student);
            return _student;
        }
    }
}
