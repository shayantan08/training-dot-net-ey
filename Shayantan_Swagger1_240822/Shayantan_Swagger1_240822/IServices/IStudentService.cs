﻿using Shayantan_Swagger1_240822.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_Swagger1_240822.IServices
{
    public interface IStudentService
    {
        List<Student> Gets();
        Student Get(int id);
        List<Student> Save(Student student);
        List<Student> Delete(int studentid);
    }
}
