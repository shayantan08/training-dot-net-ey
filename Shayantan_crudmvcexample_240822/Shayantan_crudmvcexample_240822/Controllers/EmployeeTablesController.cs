﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shayantan_crudmvcexample_240822.Models;

namespace Shayantan_crudmvcexample_240822.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeTablesController : ControllerBase
    {
        private readonly TestDBContext _context;

        public EmployeeTablesController(TestDBContext context)
        {
            _context = context;
        }

        // GET: api/EmployeeTables
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmployeeTable>>> GetEmployeeTable()
        {
            return await _context.EmployeeTable.ToListAsync();
        }

        // GET: api/EmployeeTables/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeeTable>> GetEmployeeTable(int id)
        {
            var employeeTable = await _context.EmployeeTable.FindAsync(id);

            if (employeeTable == null)
            {
                return NotFound();
            }

            return employeeTable;
        }

        // PUT: api/EmployeeTables/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployeeTable(int id, EmployeeTable employeeTable)
        {
            if (id != employeeTable.Eid)
            {
                return BadRequest();
            }

            _context.Entry(employeeTable).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeTableExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EmployeeTables
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<EmployeeTable>> PostEmployeeTable(EmployeeTable employeeTable)
        {
            _context.EmployeeTable.Add(employeeTable);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EmployeeTableExists(employeeTable.Eid))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEmployeeTable", new { id = employeeTable.Eid }, employeeTable);
        }

        // DELETE: api/EmployeeTables/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<EmployeeTable>> DeleteEmployeeTable(int id)
        {
            var employeeTable = await _context.EmployeeTable.FindAsync(id);
            if (employeeTable == null)
            {
                return NotFound();
            }

            _context.EmployeeTable.Remove(employeeTable);
            await _context.SaveChangesAsync();

            return employeeTable;
        }

        private bool EmployeeTableExists(int id)
        {
            return _context.EmployeeTable.Any(e => e.Eid == id);
        }
    }
}
