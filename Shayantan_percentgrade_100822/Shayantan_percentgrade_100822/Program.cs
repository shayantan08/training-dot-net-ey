﻿using System;

namespace Shayantan_percentgrade_100822
{
    class Program
    {
        static void Main(string[] args)
        {
            int p = Convert.ToInt32(Console.ReadLine());
            int per = (p*100) / 100;
            if (per > 90)
            {
                Console.WriteLine("A");
            }
            else if ((per > 80)&&(per<=90))
            {
                Console.WriteLine("B");
            }
            else
            {
                Console.WriteLine("C");
            }
        }
    }
}
