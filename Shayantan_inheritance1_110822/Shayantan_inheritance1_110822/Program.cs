﻿using System;

namespace Shayantan_inheritance1_110822
{
    class Auto
    {
        public String name;
        public int engno;
        public void read()
        {
            name = Console.ReadLine();
            engno = Convert.ToInt32(Console.ReadLine());
        }

        
    }

    class Reading : Auto
    {
        public void disp()
        {
            Console.WriteLine("Name - " + name);
            Console.WriteLine("Engine no. - " + engno);
        }
    }
    
    
    class Program
    {
        static void Main(string[] args)
        {
            Reading a = new Reading();
            a.read();
            a.disp();
            a.name = "Greens";
            a.engno = 9700;
            a.disp();

        }
    }
}
