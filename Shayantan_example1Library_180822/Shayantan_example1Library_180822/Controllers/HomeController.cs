﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shayantan_example1Library_180822.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_example1Library_180822.Controllers
{
   
    public class HomeController : Controller
    {
        LibClass lib = new LibClass()
        {
            Bid = 1,
            Bname = "ABC",
            counterno = 43
        };

        PersonClass per = new PersonClass()
        {
            Sid = System.Guid.NewGuid(),
            Sname = "Sam"


        };
        
    private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewBag.libra = lib;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public ActionResult Details()
        {
            ViewBag.libra = lib;
            //ViewBag.pers = per;
            
            return View();
        }

        public ActionResult Details1()
        {
            //ViewBag.libra = lib;
            ViewBag.pers = per;

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
