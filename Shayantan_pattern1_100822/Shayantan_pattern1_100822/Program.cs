﻿using System;

namespace Shayantan_pattern1_100822
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = 1; i <= 4; i++)
            {
                for(int j=1; j <= i; j++)
                {
                    Console.Write(j + " ");
                }
                Console.Write("\n");
            }
        }
    }
}
