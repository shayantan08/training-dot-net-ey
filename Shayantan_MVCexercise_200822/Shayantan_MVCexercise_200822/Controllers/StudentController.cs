﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Shayantan_MVCexercise_200822.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_MVCexercise_200822.Controllers
{
    public class StudentController : Controller
    {
        private static List<StudentModel> student = new List<StudentModel>()
        {
            new StudentModel {Sid = Guid.NewGuid(), Sname = "Sam",dob = new DateTime(2017,01,01).Date,role = "Analyst",doj = new DateTime(2020,01,01)},
            new StudentModel {Sid = Guid.NewGuid(), Sname = "Sam",dob = new DateTime(2017,01,01).Date,role = "Analyst",doj = new DateTime(2020,01,01)},
        };
        public IActionResult Index()
        {
            return View();
        }

        public ActionResult Details()
        {
            return View(student);
        }

        public ActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]

        public ActionResult AddStudent(StudentModel input)
        {
            student.Add(new StudentModel { Sid = Guid.NewGuid(), Sname = input.Sname, dob = input.dob, role = input.role, doj = input.doj });
            return View();
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            student.Remove(student.SingleOrDefault(o => o.Sid == id));
            return RedirectToAction("Details");
            ViewBag.message = "Edited Successfully";
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            return View(student.Single(o=>o.Sid == id));
        }

        [HttpPost]

        public ActionResult Edit(Guid id, StudentModel input)
        {
            //var stude = student.Where(x => x.Sid == s.Sid).Sig();
            var e = student.Where(x => x.Sid == id).SingleOrDefault();
            student.Remove(e);
            student.Add(new StudentModel { Sid = id, Sname = input.Sname, dob = input.dob, role = input.role, doj = input.doj });

            return View();
        }
    }
}
