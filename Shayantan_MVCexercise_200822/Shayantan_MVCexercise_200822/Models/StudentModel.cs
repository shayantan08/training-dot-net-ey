﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_MVCexercise_200822.Models
{
    public class StudentModel
    {
        public Guid Sid { get; set; }
        public string Sname { get; set; }
        public DateTime dob { get; set; }
        public string role { get; set; }
        public DateTime doj { get; set; }

    }
}
