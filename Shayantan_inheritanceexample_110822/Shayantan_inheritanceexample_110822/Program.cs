﻿using System;

namespace Shayantan_inheritanceexample_110822
{
    class vehicle
    {
        public string name;
        
    }

    class car : vehicle
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("Cars");
        }
    }

    class student
    {
        public string name;

    }

    class cl:student
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("student");
        }
    }

    class theatre
    {
        public string name;

    }

    class movies:theatre
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("theatre");
        }
    }

    class apps
    {
        public string name;

    }

    class playstore:apps
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("apps");
        }
    }

    class books
    {
        public string name;

    }

    class library:books
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("books");
        }
    }

    class aeroplane
    {
        public string name;

    }

    class airports : aeroplane
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("aeroplane");
        }
    }

    class food
    {
        public string name;

    }

    class canteen:food
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("food");
        }
    }

    class cricket
    {
        public string name;

    }

    class sports:cricket
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("food");
        }
    }

    class treadmill
    {
        public string name;

    }

    class gym:treadmill
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("cricket");
        }
    }

    class mobile
    {
        public string name;

    }

    class electronics:mobile
    {
        public void disp1()
        {
            //name = Console.ReadLine();
            Console.WriteLine("mobile");
        }
    }

    static class laptop
    {
        public static String name = "Hello"; //cannot read it as class is static

    }

    sealed class mouse
    {
        public int a = 10;
    }

    class A : mouse //shows error as parent class is sealed 
    {
        public void disp1()
        {
            Console.WriteLine(name);        // shows error as variable cant be accessed due to sealed property
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            car v = new car(); v.disp1();
            cl s = new cl(); s.disp1();
            movies m = new movies(); m.disp1();
            playstore ap = new playstore(); ap.disp1();
            library b = new library(); b.disp1();
            airports ae = new airports(); ae.disp1();
            canteen f = new canteen(); f.disp1();
            sports c = new sports(); c.disp1();
            gym t = new gym(); t.disp1();
            electronics me = new electronics(); me.disp1();
        }
    }
}
