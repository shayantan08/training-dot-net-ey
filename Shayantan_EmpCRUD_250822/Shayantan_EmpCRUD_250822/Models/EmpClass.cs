﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_EmpCRUD_250822.Models
{
    public class EmpClass
    {
        [Required]
        public int eid { get; set; }
        public string ename { get; set; }
        public string Dept { get; set; }
    }
}
