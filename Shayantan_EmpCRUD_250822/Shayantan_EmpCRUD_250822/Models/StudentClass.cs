﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_EmpCRUD_250822.Models
{
    public class StudentClass
    {
        public int Sid { get; set; }
        public string Sname { get; set; }
        public string Dept { get; set; }
    }
}
