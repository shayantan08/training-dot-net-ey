﻿using Shayantan_EmpCRUD_250822.IServices;
using Shayantan_EmpCRUD_250822.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_EmpCRUD_250822.Services
{
    public class EmpService : IEmpService
    {
        List<EmpClass> _student = new List<EmpClass>();
        public EmpService()
        {
            for (int i = 0; i < 3; i++)
            {
                _student.Add(new EmpClass()
                {
                    eid = i,
                    ename = "Emp" + i,
                    Dept = "100" + i

                });

            }
        }
            public List<EmpClass> Delete(int id)
        {
                _student.RemoveAll(x => x.eid == id);
                return _student;
            }

        public EmpClass Getsingle(int id)
        {
            return _student.SingleOrDefault(x => x.eid == id);
        }

        public List<EmpClass> Gets()
        {
            return _student;
        }

        public List<EmpClass> Save(EmpClass emp)
        {
            _student.Add(emp);
            return _student;
        }
    }
}
