﻿using Shayantan_EmpCRUD_250822.IServices;
using Shayantan_EmpCRUD_250822.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_EmpCRUD_250822.Services
{
    public class StudentServices : IStudentServices
    {
        List<StudentClass> _stud = new List<StudentClass>();
        public StudentServices()
        {
            for(int i = 0; i < 3; i++)
            {
                _stud.Add(new StudentClass()
                {
                    Sid = i,
                    Sname = "Student"+i,
                    Dept = "Section"+i

                });
                   
            }
            
        }
        public List<StudentClass> Delete(int id)
        {
            _stud.RemoveAll(c => c.Sid == id);
            return _stud;
        }

        public List<StudentClass> Gets()
        {
            return _stud;
        }

        public StudentClass GetSingle(int id)
        {
            return _stud.SingleOrDefault(c => c.Sid == id);
        }

        public List<StudentClass> Save(StudentClass stud)
        {
            _stud.Add(stud);
            return _stud;
        }
    }
}
