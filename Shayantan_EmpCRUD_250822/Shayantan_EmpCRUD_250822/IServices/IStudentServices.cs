﻿using Shayantan_EmpCRUD_250822.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_EmpCRUD_250822.IServices
{
    public interface IStudentServices
    {
        List<StudentClass> Gets();
        StudentClass GetSingle(int id);
        List<StudentClass> Save(StudentClass stud);
        List<StudentClass> Delete(int id);
    }
}
