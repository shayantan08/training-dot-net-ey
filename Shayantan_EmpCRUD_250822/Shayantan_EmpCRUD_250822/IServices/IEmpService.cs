﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shayantan_EmpCRUD_250822.Models;

namespace Shayantan_EmpCRUD_250822.IServices
{
    public interface IEmpService
    {
        List<EmpClass> Gets();
        EmpClass Getsingle(int id);
        List<EmpClass> Save(EmpClass emp);
        List<EmpClass> Delete(int id);
    }
}
