﻿using Microsoft.AspNetCore.Mvc;
using Shayantan_EmpCRUD_250822.IServices;
using Shayantan_EmpCRUD_250822.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Shayantan_EmpCRUD_250822.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpController : ControllerBase
    {
        IEmpService _emp;
        public EmpController(IEmpService emp)
        {
            _emp = emp;
        }
        
        // GET: api/<EmpController>
        [HttpGet]
        public List<EmpClass> Get()
        {
            return _emp.Gets();
        }

        // GET api/<EmpController>/5
        [HttpGet("{id}",Name ="Get")]
        public EmpClass Get(int id)
        {
            return _emp.Getsingle(id);
        }

        // POST api/<EmpController>
        [HttpPost]
        public List<EmpClass> Post([FromBody] EmpClass emp)
        {
            return _emp.Save(emp);
        }

        // PUT api/<EmpController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EmpController>/5
        [HttpDelete("{id}")]
        public List<EmpClass> Delete(int id)
        {
            return _emp.Delete(id);
        }
    }
}
