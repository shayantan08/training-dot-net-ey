﻿using Microsoft.AspNetCore.Mvc;
using Shayantan_EmpCRUD_250822.IServices;
using Shayantan_EmpCRUD_250822.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Shayantan_EmpCRUD_250822.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        IStudentServices _stud;
        public StudentController(IStudentServices stud)
        {
            _stud = stud;
        }
        
        // GET: api/<StudentController>
        [HttpGet]
        public List<StudentClass> Get()
        {
            return _stud.Gets();
        }

        // GET api/<StudentController>/5
        [HttpGet("{id}")]
        public StudentClass Get(int id)
        {
            return _stud.GetSingle(id);
        }

        // POST api/<StudentController>
        [HttpPost]
        public List<StudentClass> Post([FromBody] StudentClass stud)
        {
            return _stud.Save(stud);
        }

        // PUT api/<StudentController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<StudentController>/5
        [HttpDelete("{id}")]
        public List<StudentClass> Delete(int id)
        {
            return _stud.Delete(id);
        }
    }
}
