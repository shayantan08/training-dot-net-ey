﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shayantan_MVC2_190822.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_MVC2_190822.Controllers
{
    public class HomeController : Controller
    {
        
        private readonly ILogger<HomeController> _logger;
        private static List<StudentClass> student = new List<StudentClass>()
        {
            new StudentClass {Sid = Guid.NewGuid(), Sname = "Sam"},
            new StudentClass {Sid = Guid.NewGuid(), Sname = "Sam"},
            new StudentClass {Sid = Guid.NewGuid(), Sname = "Sam"}
        };
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public ActionResult Details1()
        {
            return View(student);
        }

        public ActionResult AddStudent()
        {
            return View();
        }
        [HttpPost]

        public ActionResult AddStudent(StudentClass input)
        {
            student.Add(new StudentClass { Sid = Guid.NewGuid(), Sname = input.Sname });
            return View();
        }
        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            student.Remove(student.SingleOrDefault(o => o.Sid == id));
            return RedirectToAction("Details1");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
