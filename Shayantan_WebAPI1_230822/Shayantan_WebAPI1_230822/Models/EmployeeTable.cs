﻿using System;
using System.Collections.Generic;

namespace Shayantan_WebAPI1_230822.Models
{
    public partial class EmployeeTable
    {
        public int Eid { get; set; }
        public string Ename { get; set; }
        public string Dept { get; set; }
    }
}
