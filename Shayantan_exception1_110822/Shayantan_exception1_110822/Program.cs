﻿using System;

namespace Shayantan_exception1_110822
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int i = 1;
                int j = i / 0;
                int[] ab = new int[4];

             
            }

            catch(DivideByZeroException e)
            {
                Console.WriteLine("Divide by zero attempted \n{0}", e.StackTrace);
            }

            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine("Error \n{0}", ex.StackTrace);
            }

            finally
            {
                Console.WriteLine("Improve your error");
            }
        }
    }
}
