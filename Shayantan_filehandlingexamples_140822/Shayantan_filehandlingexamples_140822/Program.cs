﻿using System;
using System.IO;

namespace Shayantan_filehandlingexamples_140822
{
    class Program
    {
        class FileWrite
        {
            public void WriteData()
            {
                
                FileStream fs1 = new FileStream("e:\\file1.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw1 = new StreamWriter(fs1);
                Console.WriteLine("Enter the text which you want to write to the file");
                string str = Console.ReadLine();
                sw1.WriteLine(str);
                sw1.Flush();
                sw1.Close();
                fs1.Close();
            }

            public void Read()
            {
                using (FileStream fs = File.Open("e:\\hello.txt", FileMode.Open))
                {
                    byte[] bytesRead = new byte[fs.Length];
                    fs.Read(bytesRead, 0, Convert.ToInt32(fs.Length));

                    string result = System.Text.Encoding.UTF8.GetString(bytesRead);
                    Console.Write("Contents written in hello - " + result);

                }

                

            }

            public void copy()
            {
                File.Copy("e:\\file1.txt", "e:\\file2.txt", true);
                Console.Write("File copied");
            }

        }
        static void Main(string[] args)
        {
            FileWrite ob1 = new FileWrite();
            ob1.WriteData();
            ob1.Read();
            ob1.copy();
        }
    }
}
