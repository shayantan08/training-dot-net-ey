﻿using System;

namespace Shayantan_Stringoperations_100822
{
    class Program
    {
        static void Main(string[] args)
        {
            String str = "Hello";
            String str1 = "World";
            String str3 = " Sam Halls ";

            Console.WriteLine(str1.CompareTo(str));
            Console.WriteLine(str1.Equals(str));
            Console.WriteLine(str.ToLower());
            Console.WriteLine(str.ToUpper());
            Console.WriteLine(str.Length);
            Console.WriteLine(str.Replace("e", "b"));
            Console.WriteLine(str3.Remove(5));
            Console.WriteLine(str3.LastIndexOf("m"));
            Console.WriteLine(str3.Trim());
            Console.WriteLine(String.Concat(str, str1));



        }
    }
}
