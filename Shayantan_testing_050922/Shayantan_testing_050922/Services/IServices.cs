﻿using Shayantan_testing_050922.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_testing_050922.Services
{
    public interface IServices
    {
        List<EmployeeTable> GetStudents();
        EmployeeTable AddStudents(EmployeeTable employee);
    }
}
