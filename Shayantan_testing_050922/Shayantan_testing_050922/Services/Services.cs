﻿using Shayantan_testing_050922.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_testing_050922.Services
{
    public class Services : IServices
    {
        private List<EmployeeTable> emp = new List<EmployeeTable>();
        public TestDBContext _data;
        public Services(TestDBContext data)
        {
            _data = data;
        }
        public EmployeeTable AddStudents(EmployeeTable employee)
        {
            var Emplist = _data.EmployeeTable.ToList();
            Emplist.Add(employee);
            return employee;
        }

        public List<EmployeeTable> GetStudents()
        {
            return _data.EmployeeTable.ToList();
        }
    }
}
