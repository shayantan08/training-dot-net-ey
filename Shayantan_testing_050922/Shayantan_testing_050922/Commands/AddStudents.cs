﻿using MediatR;
using Shayantan_testing_050922.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_testing_050922.Commands
{
    public class AddStudents:IRequest<EmployeeTable>{
        public EmployeeTable EmployeeTable { get; set; }
}
}
