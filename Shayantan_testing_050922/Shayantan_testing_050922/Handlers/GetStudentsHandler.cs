﻿using MediatR;
using Shayantan_testing_050922.Models;
using Shayantan_testing_050922.Queries;
using Shayantan_testing_050922.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_testing_050922.Handlers
{
    public class GetStudentsHandler : IRequestHandler<GetStudents, List<EmployeeTable>>
    {
        private readonly IServices _data;
        public GetStudentsHandler(IServices data){
            _data = data;
        }
        Task<List<EmployeeTable>> IRequestHandler<GetStudents, List<EmployeeTable>>.Handle(GetStudents request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.GetStudents());
        }
    }
}
