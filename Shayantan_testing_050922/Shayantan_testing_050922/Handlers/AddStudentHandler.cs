﻿using MediatR;
using Shayantan_testing_050922.Commands;
using Shayantan_testing_050922.Models;
using Shayantan_testing_050922.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Shayantan_testing_050922.Handlers
{
    public class AddStudentHandler : IRequestHandler<AddStudents, EmployeeTable>
    {
        private readonly IServices _data;
        public AddStudentHandler(IServices data)
        {
            _data=data;
        }
        public Task<EmployeeTable> Handle(AddStudents request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.AddStudents(request.e, request.lastName, request.age));
        }
    }
}
