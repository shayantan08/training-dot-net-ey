﻿using System;
using System.Collections.Generic;

namespace Shayantan_testing_050922.Models
{
    public partial class EmployeeTable
    {
        public int Eid { get; set; }
        public string Ename { get; set; }
        public string Dept { get; set; }
    }
}
