﻿using System;
using System.Threading;

namespace Shayantan_threads1_120822
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(new ThreadStart(work1));
            Thread t2 = new Thread(new ThreadStart(work2));
            t1.Start();
            t2.Start();


        }
        public static void work1()
        {
            for(int i = 1; i < 5; i++)
            {
                Console.WriteLine(i);
            }
        }

        public static void work2()
        {
            for (int j = 9; j < 12; j++)
            {
                Console.WriteLine(j);
            }
        }
    }

    
}
