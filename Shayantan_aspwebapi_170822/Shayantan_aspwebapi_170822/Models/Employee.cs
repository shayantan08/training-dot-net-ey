﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shayantan_aspwebapi_170822.Models
{
    public class Employee
    {
        public int Empid { get; set; }
        public string Ename { get; set; }
        public int deptno { get; set; }
        public int sal { get; set; }

    }
}
