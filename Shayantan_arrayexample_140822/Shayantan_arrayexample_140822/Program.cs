﻿using System;
using System.Collections;


namespace Shayantan_arrayexample_140822
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList ar1 = new ArrayList();
            ar1.Add(1);
            ar1.Add("Sam");
            ar1.Add(24.05);

            foreach (var i in ar1)
                Console.Write(i + ",");

            Console.Write("\n");
            ar1.Remove("Sam");
            foreach (var i in ar1)
                Console.Write(i + ",");

            Console.Write("\n");
            ar1.Add("Shaun");
            ar1.RemoveAt(0);
            foreach (var i in ar1)
                Console.Write(i + ",");

            Console.Write("\n");
            Console.WriteLine(ar1.Contains("Sam"));
            Console.WriteLine(ar1.IndexOf("Shaun"));

        }
    }
}
