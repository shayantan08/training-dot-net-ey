﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shayantan_CRUD1_220822.Models
{
    public partial class EmployeeTable
    {
        [Required]
        public int Eid { get; set; }
        [Required]
        public string Ename { get; set; }
        public string Dept { get; set; }
    }
}
