﻿using System;

namespace Shayantan_abstractexercise_110822
{
    abstract public class vehicle
    {
        public String name = "cars";

        public abstract void show();
    }

    public class car:vehicle
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    abstract public class cl
    {
        public String name = "books";

        public abstract void show();
    }

    public class students : cl
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    abstract public class theatre
    {
        public String name = "movies";

        public abstract void show();
    }

    public class movies : theatre
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    abstract public class playstore
    {
        public String name = "apps";

        public abstract void show();
    }

    public class apps : playstore
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }

    abstract public class library
    {
        public String name = "books";

        public abstract void show();
    }

    public class books : library
    {
        public override void show()
        {
            Console.WriteLine(name);
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            books b = new books(); b.show();
            car v = new car(); v.show();
            movies m = new movies(); m.show();
            apps a = new apps(); a.show();
            students st = new students(); st.show();
        }
    }
}
